# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import *
from django.http import QueryDict
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from user_auth.views import authenticate
from utils import *
from user_auth.models import Pengguna, Status, Keahlian
from django.urls import reverse

response = {}
def index(request): # pragma: no cover
    if not authenticate(request):
        return HttpResponseRedirect(reverse('user_auth:index'))
    else:
        kode_identitas = get_data_user(request, 'kode_identitas')
        user_exist = check_user_in_database(request, kode_identitas)
        if (user_exist):
            pengguna = Pengguna.objects.get(npm = kode_identitas)
        else:
            pengguna = create_new_user(request)
        response['user'] = pengguna
        if Status.objects.filter(pengguna=pengguna).count() > 0:
            latest_status = Status.objects.filter(pengguna=pengguna).latest('created_date')
            response['latest_status'] = latest_status
            status_count = Status.objects.filter(pengguna=pengguna).count()
            response['status_count'] = status_count
        html = 'profile/profile.html'
        response['menuSelected'] = 'profile'
        return render(request, html, response)

def edit_profile(request): # pragma: no cover
    if not authenticate(request):
        return HttpResponseRedirect(reverse('user_auth:index'))
    else:
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(npm = kode_identitas)
        if Status.objects.filter(pengguna=pengguna).count() > 0:
            latest_status = Status.objects.filter(pengguna=pengguna).latest('created_date')
            response['latest_status'] = latest_status
            status_count = Status.objects.filter(pengguna=pengguna).count()
            response['status_count'] = status_count
        response['user'] = pengguna
        skill = Keahlian.objects.filter(pengguna=pengguna)
        response['skill'] = skill
        response['connected'] = check_user_connected_linkedin(request, kode_identitas)
        html = 'profile/edit.html'
        return render(request, html, response)

@csrf_exempt
def update_profile(request): # pragma: no cover
    if request.method == 'POST':
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(npm = kode_identitas)
        fullname = request.POST['full_name']
        url_linkedin = request.POST['url_linkedin']
        picture_url = request.POST['picture_url']
        email = request.POST['email']
        headline = request.POST['headline']
        pengguna.nama = fullname
        pengguna.url_linkedin = url_linkedin
        pengguna.foto = picture_url
        pengguna.email = email
        pengguna.headline = headline
        pengguna.save()
        return HttpResponseRedirect(reverse('profile:edit-profile'))
@csrf_exempt
def edit_profile_local(request): # pragma: no cover
    if request.method == 'POST':
        request.POST = request.POST.copy()
        show = request.POST['show_score']
        keahlian = request.POST.getlist('skill')
        level = request.POST.getlist('level')
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(npm = kode_identitas)
        counter = 0;
        for a in keahlian:
            if (Keahlian.objects.filter(pengguna=pengguna,keahlian=a)):
                keahlianObj = Keahlian.objects.get(pengguna=pengguna,keahlian=a)
                keahlianObj.keahlian = a
                keahlianObj.level = level[counter]
            else:
                keahlianObj = Keahlian.objects.create(pengguna=pengguna, keahlian=a,level=level[counter])
            keahlianObj.save()
            counter += 1
        pengguna.show_score = show
        pengguna.save()
    return HttpResponseRedirect(reverse('profile:edit-profile'))

def delete_skill(request,skillname):# pragma: no cover
    print('=====>delete')
    if request.method == 'GET':
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(npm = kode_identitas)
        Keahlian.objects.get(pengguna=pengguna,keahlian = skillname).delete()
        return HttpResponseRedirect(reverse('profile:edit-profile-local'))

# Birthday
# Gender
# Expertise : list
# Description
# Email
