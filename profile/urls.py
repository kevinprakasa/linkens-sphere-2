from django.conf.urls import url
from .views import index, edit_profile, update_profile, edit_profile_local, delete_skill


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit/', edit_profile, name='edit-profile'),
    url(r'^update-profile/$', update_profile, name='update-profile'),
    url(r'^edit-profile-local/$', edit_profile_local, name='edit-profile-local'),
    url(r'^delete-skill/(?P<skillname>[a-zA-Z]+)$', delete_skill, name='delete-skill')

]
