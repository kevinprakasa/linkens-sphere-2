# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.urls import resolve
from django.contrib.auth.models import *
from .views import *
from .models import Profile
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class ViewProfileUnitTest(TestCase):
    def test_profile_url_is_exist(self):
         response = Client().get('/profile/')
         self.assertEqual(response.status_code, 302)
    def test_profile_using_index_func(self):
         found = resolve('/profile/')
         self.assertEqual(found.func, index)

