from django.shortcuts import render
from user_auth.models import Status,Pengguna
from user_auth.views import authenticate
from user_auth.forms import Status_Form
from utils import *
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
# Create your views here.

response = {}
def index(request): # pragma: no cover
    if not authenticate(request):
        return httpResponseRedirect(reverse('user_auth:index'))
    else:
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(npm = kode_identitas)
        response['user'] = pengguna
        response['status_form'] = Status_Form
        message = Status.objects.filter(pengguna=pengguna).order_by('-created_date')
        response['status'] = message
        if Status.objects.filter(pengguna=pengguna).count() > 0:
            latest_status = Status.objects.filter(pengguna=pengguna).latest('created_date')
            response['latest_status'] = latest_status
            status_count = Status.objects.filter(pengguna=pengguna).count()
            response['status_count'] = status_count
        response['menuSelected'] = 'status'
        html = 'status.html'
        return render(request, html, response)

def add_message(request): # pragma: no cover
    if authenticate(request):
        form = Status_Form(request.POST or None)
        if(request.method == 'POST' and form.is_valid() ):
            response['message'] = request.POST['message']
            kode_identitas = get_data_user(request, 'kode_identitas')
            pengguna = Pengguna.objects.get(npm = kode_identitas)
            status = Status(pengguna=pengguna, message=response['message'])
            status.save()
            return HttpResponseRedirect(reverse('status_page:index'))
        else:
            return HttpResponseRedirect(reverse('status_page:index'))
    else:
        return HttpResponse('Unauthorized', status=401)