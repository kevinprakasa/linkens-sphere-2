from django.conf.urls import url
from .views import index, add_message
# from .views import index, add_message, add_comment, index_comment

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add_message', add_message, name='add_message'),
	# url(r'^add_comment', index_comment, name='add_comment'),
	# url(r'^save_comment/(?P<pk>[0-9]+)/$', add_comment, name='save_comment'),
]