"""linkensphere URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import profile.urls as profile
from profile.views import index as index_profile
from django.views.generic import TemplateView
import riwayat.urls as riwayat
import user_auth.urls as user_auth
import status_page.urls as status_page
import cari.urls as cari


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile/', include(profile, namespace='profile')),
    url(r'^profile/status/', include(status_page, namespace='status_page')),
    url(r'^$', TemplateView.as_view(template_name='landingPage.html'),name='landingpage'),
    url(r'^$', RedirectView.as_view(url='/profile/', permanent=True), name='index'),
    url(r'^riwayat/', include(riwayat, namespace='riwayat')),
    url(r'^user_auth/', include(user_auth, namespace='user_auth')),
	url(r'^profile/teman/', include(cari, namespace='cari'))

]
#url
