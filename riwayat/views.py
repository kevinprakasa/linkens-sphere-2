from django.shortcuts import render
import requests
from django.urls import reverse
from django.http import *
from user_auth.views import authenticate
from utils import *
from user_auth.models import Pengguna, Status

# Create your views here.

STUDENT = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'
def index(request): # pragma: no cover
    if not authenticate(request):
        return HttpResponseRedirect(reverse('user_auth:index'))
    else:
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(npm = kode_identitas)
        user = Pengguna.objects.get(npm = kode_identitas)
        student = requests.get(STUDENT).json()
        show_nilai = user.show_score
        response = {'matkul':student, 'show_nilai':show_nilai, 'menuSelected':'riwayat', 'user':pengguna}
        if Status.objects.filter(pengguna=pengguna).count() > 0:
            latest_status = Status.objects.filter(pengguna=pengguna).latest('created_date')
            status_count = Status.objects.filter(pengguna=pengguna).count()
            response['latest_status'] = latest_status
            response['status_count'] = status_count
        html = 'riwayat.html'
        return render(request, html, response)
