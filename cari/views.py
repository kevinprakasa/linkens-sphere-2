from django.shortcuts import render
from user_auth.models import Pengguna, Keahlian,Status
from utils import *
# Create your views here.

response = {}
def index(request):# pragma: no cover
	kode_identitas = get_data_user(request, 'kode_identitas')
	pengguna = Pengguna.objects.get(npm = kode_identitas)
	response['user'] = pengguna
	message = Status.objects.order_by('-created_date')
	response['status'] = message
	if Status.objects.filter(pengguna=pengguna).count() > 0:
	    latest_status = Status.objects.filter(pengguna=pengguna).latest('created_date')
	    response['latest_status'] = latest_status
	    status_count = Status.objects.filter(pengguna=pengguna).count()
	    response['status_count'] = status_count
	response['menuSelected'] = 'teman'
	html = 'cari/teman/teman.html'
	pengguna = Pengguna.objects.all()
	keahlian = []
	for u in pengguna:
		skillQuery = Keahlian.objects.filter(pengguna=u)
		temp_str = ""
		counter = 0 
		for x in list(skillQuery):
			temp_str += x.keahlian+","
			if(counter == len(list(skillQuery))-1):
				temp_str = temp_str[:-1]
			counter+=1
		keahlian.append(temp_str)
	keahlianPengguna = zip(keahlian,pengguna)
	response['keahlian'] = keahlian
	response['pengguna'] = pengguna
	response['keahlianPengguna'] = keahlianPengguna
	return render(request, html, response)