from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

# Create your tests here.
class CariUnitTest(TestCase):

	def test_cari_using_index_func(self):
		found = resolve('/profile/teman/')
		self.assertEqual(found.func, index)