from user_auth.models import Pengguna


def check_user_in_database(request, npm): # pragma: no cover
    is_exist = False
    kode_identitas = get_data_user(request, 'kode_identitas')
    count_pengguna = Pengguna.objects.filter(npm=kode_identitas).count()
    if count_pengguna > 0 :
        is_exist = True

    return is_exist

def check_user_connected_linkedin(request, npm): # pragma: no cover
    connected = False
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(npm=kode_identitas)

    if pengguna.nama:
        connected = True

    return connected

def get_data_user(request, tipe): # pragma: no cover
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data
def create_new_user(request): # pragma: no cover
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna() #only get npm from sso auth
    pengguna.npm = kode_identitas
    pengguna.save()

    return pengguna