from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'This field is required',
    }
    attrs = {
        'class': 'form',
		'placeholder': 'What is on your mind?'
    }
    message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True, label='')