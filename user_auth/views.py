from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
# from .models import Pengguna, Keahlian, Status
# Create your views here.
response = {}

def index(request): # pragma: no cover
    # print ("#==> masuk index")
    if 'user_login' in request.session:
    	return HttpResponseRedirect((reverse("profile:index")))
    else:
        response['author'] = "kevin"
        html = 'login.html'
        return render(request, html, response)

def authenticate(request): # pragma: no cover
	return ('user_login' in request.session.keys())

def create_new_user(request): # pragma: no cover
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.save()

    return pengguna
def showLandingPage(request): # pragma: no cover
    html = ""
    return render(request,html,response)

def get_data_user(request, tipe): # pragma: no cover
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']
    return data