from django.db import models

# Create your models here.
class Pengguna(models.Model):
	nama = models.CharField(max_length=100,null=True)
	npm = models.CharField(max_length=20)
	url_linkedin = models.URLField(null=True)
	foto = models.URLField(null=True)
	email = models.EmailField(max_length=200,null=True)
	show_score = models.BooleanField(default=True)
	headline = models.CharField(max_length=300,null=True)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)

class Keahlian(models.Model):
	pengguna = models.ForeignKey(Pengguna, on_delete=models.CASCADE)
	java = 'ja'
	c = 'c'
	python = 'py'
	kotlin = 'ko'
	keahlian_choices = (
		(java, 'Java'),
		(c, 'C#'),
		(python, 'Python'),
		(kotlin, 'Kotlin'),
	)
	keahlian = models.CharField(
		max_length=2,
		choices=keahlian_choices,
		)
	beginner = 'be'
	intermediate = 'in'
	advanced = 'ad'
	expert = 'ex'
	legend = 'le'
	level_choices = (
		(beginner, 'Beginner'),
		(intermediate, 'Intermediate'),
		(advanced, 'Advanced'),
		(expert, 'Expert'),
		(legend, 'Legend'),
		)
	level = models.CharField(
		max_length=2,
		choices=level_choices,
		)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)


class Status(models.Model):
	pengguna = models.ForeignKey(Pengguna, on_delete=models.CASCADE)
	message = models.CharField(max_length=128)
	created_date = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)

