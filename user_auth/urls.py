from django.conf.urls import url
from .views import *

from custom_auth import auth_login, auth_logout

urlpatterns = [
    # custom auth
    url(r'^login/$', auth_login, name='auth_login'),
    url(r'^logout/$', auth_logout, name='auth_logout'),
    url(r'^$', index, name='index'),

]
